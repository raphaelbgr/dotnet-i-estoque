﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace estoque
{
    public class Produto
    {
        private int codigo; public int Codigo { get { return codigo; } set { codigo = value; } }
        private string descricao; public string Descricao { get { return descricao; } set { descricao = value; } }
        private string fornecedor; public string Fornecedor { get { return fornecedor; } set { fornecedor = value; } }
        private string validade; public string Validade { get { return this.validade; } set { this.validade = value; } }
        private int qtdEstoque; public int QtdEstoque { get { return this.qtdEstoque; } set { this.qtdEstoque = value; } }
        private double precoForn; public double PrecoForn { get { return this.precoForn; } set { this.precoForn = value; } }
        private double precoFinal; public double PrecoFinal { get { return precoFinal; } set { this.precoFinal = value; } }
        private int qtd; public int Qtd { get { return this.qtd; } set { this.qtd = value; } }

    }
}
