﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace estoque
{
    public partial class LoginController : Form
    {
        PainelController pc;
        private CadastroDB cdb;

        public LoginController()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cdb == null)
            {
                cdb = new CadastroDB();
            }

            Operador op = cdb.Autenticar(textBox1.Text, textBox2.Text);
            if (op != null)
            {
                Hide();
                pc = new PainelController(cdb,this,op);
                pc.Show();
            }
            else
            {
                MessageBox.Show("Login Falha - usuário não encontrado.");
            }
        }

        private void LoginController_Load(object sender, EventArgs e)
        {

        }
    }
}
