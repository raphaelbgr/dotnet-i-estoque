﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace estoque
{
    class ProdutosDB
    {
        public Produto getProdutoByID(int id)
        {
            Produto p = null;
            using (SqlConnection sqlConn = new SqlConnection("Data Source=(localdb)\\mssqllocaldb;Initial Catalog=estoque;Integrated Security=True"))
            {
                try
                {
                    using (SqlCommand sqlCommand = new SqlCommand())
                    {
                        sqlCommand.Parameters.AddWithValue("@ID", id);
                        sqlCommand.CommandText = "SELECT * FROM PRODUTOS WHERE COD = @ID";
                        sqlCommand.Connection = sqlConn;

                        sqlConn.Open();

                        SqlDataReader dataReader = sqlCommand.ExecuteReader(); //para caso que retorna registros

                        if (dataReader.Read()) //encontrou registro
                        {
                            p = new Produto();
                            p.Codigo = Int32.Parse(dataReader["COD"].ToString());
                            p.Descricao = dataReader["DESCRICAO"].ToString();
                            p.Fornecedor = dataReader["FORNECEDOR"].ToString();
                            p.Validade = dataReader["VALIDADE"].ToString();
                            p.PrecoForn = Double.Parse(dataReader["PRECOCOMPRA"].ToString());
                            p.PrecoFinal = Double.Parse(dataReader["PRECOVENDA"].ToString());
                            p.QtdEstoque = Int32.Parse(dataReader["QTDESTOQUE"].ToString());
                        }
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Não foi possível conectar ao banco, razão: " + e.Message);
                }
                finally
                {
                    sqlConn.Close(); // Fecha a conexão
                }
                return p;
            }
        }
    }
}
