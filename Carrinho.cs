﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace estoque
{
    class CarrinhoDB
    {
        private List<Produto> lista; public List<Produto> Lista { get { return lista; } set { lista = value; } }
        private int operador; public int Operador { get { return operador; } set { operador = value; } }
        private double precoTotal; public double Preco { get { return precoTotal;  } set { precoTotal = value; } }
        private int qtd; public int Qtd { get { return qtd; } set { qtd = value; } }

        public void addProduto(Produto p)
        {
            bool exists = false;
            foreach (Produto p2 in Lista)
            {
                if (p.Codigo == p2.Codigo)
                {
                    p2.Qtd = p2.Qtd + 1;
                    exists = true;
                }
            }
            if (!exists)
            {
                p.Qtd = 1;
                Lista.Add(p);
            }
            updateCarrinhoAttr();
        }

        public void removeProduto(Produto p)
        {
            foreach(Produto p2 in Lista)
            {
                if (p.Codigo == p2.Codigo)
                {
                    Lista.Remove(p2);
                }
            }
        }

        public void updateCarrinhoAttr()
        {
            Qtd = 0;
            Preco = 0;
            foreach (Produto p in Lista)
            {
                Qtd++;
                Preco = Preco + p.PrecoFinal * p.Qtd;
            }
        }
        public CarrinhoDB(Operador op)
        {
            Lista = new List<Produto>();
            Operador = op.Id;
        }

        public void limparCarrinho()
        {
            Lista.Clear();
            updateCarrinhoAttr();
        }

        public string ProductsToString()
        {
            string s = "";
            foreach (Produto p in Lista)
            {
                s = s + p.Descricao + " (x" + p.Qtd + ")" + ", ";
            }
            return s.Remove(s.Length - 2, 2);
        }
    }
    
}
