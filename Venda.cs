﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace estoque
{
    class Venda
    {
        private int codigo;
        private int codOperador;

        private double precoVenda;

        private string produtos;

        private string data;
        private string hora;

        public int Codigo
        {
            get
            {
                return codigo;
            }
            set
            {
                codigo = value;
            }
        }

        public int CodOperador
        {
            get
            {
                return codOperador;
            }
            set
            {
                codOperador = value;
            }
        }

        public double PrecoVenda
        {
            get
            {
                return precoVenda;
            }
            set
            {
                precoVenda = value;
            }
        }

        public string Produtos
        {
            get
            {
                return produtos;
            }
            set
            {
                produtos = value;
            }
        }

        public string Hora
        {
            get
            {
                return hora;
            }
            set
            {
                hora = value;
            }
        }

        public string Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        //public String printProdutos()
        //{
        //    string s = null;
        //    foreach (Produto produto in produtos)
        //    {
        //        if (s != null)
        //        {
        //            s = s + ", " + produto.Descricao;
        //        }
        //        else
        //        {
        //            s = produto.Descricao;
        //        }
        //    }
        //    return s;
        //}
    }

}
