﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace estoque
{
    public class Operador
    {
        private int id;
        private int tipo;

        private string nome;
        private string login;
        private string senha;
        private string endereco;
        private string telefone;

        public Operador(int id, int tipo, string nome, string login, string senha, string endereco, string telefone)
        {
            this.id = id;
            this.tipo = tipo;
            this.nome = nome;
            this.login = login;
            this.senha = senha;
            this.endereco = endereco;
            this.telefone = telefone;
        }

        public Operador()
        {
        }

        public int Id
        {
            get { return id; }
            set { this.id = value; }

        }

        public int Tipo
        {
            get { return tipo; }
            set { this.tipo = value; }

        }
        public string Nome
        {
            get { return nome; }
            set { this.nome = value; }

        }
        public string Login
        {
            get { return login; }
            set { this.login = value; }

        }
        public string Senha
        {
            get { return senha; }
            set { this.senha = value; }

        }
        public string Endereco
        {
            get { return endereco; }
            set { this.endereco = value; }

        }
        public string Telefone
        {
            get { return telefone; }
            set { this.telefone = value; }

        }
    }
    
}
