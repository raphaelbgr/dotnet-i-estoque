﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace estoque
{
    public partial class PainelController : Form
    {
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private LoginController lctrl;
        public Operador op;
        private Form2 form2;
        private CadastroDB cdb;
        private ProdutosDB pdb;
        private CarrinhoDB carr;
        private VendasDB vdb;


        public CadastroDB GetCadastroDB()
        {
            return cdb;
        }

        private void PainelController_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'estoqueDataSet.Produtos' table. You can move, or remove it, as needed.
            this.produtosTableAdapter.Fill(this.estoqueDataSet.Produtos);

        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            // Confirm user wants to close
            switch (MessageBox.Show(this, "Tem certeza que quer sair?", "Fechamento", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    e.Cancel = true;
                    break;
                default:
                    Dispose();
                    Application.Exit();
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (vdb == null)
            {
                vdb = new VendasDB();
            }
            vdb.RealizarVenda(carr);
            dataGridView1.DataSource = null;
            //dataGridView1.DataSource = Vendas
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                int rowindex = dataGridView3.SelectedCells[0].RowIndex;
                int pid = Int32.Parse(dataGridView3.Rows[rowindex].Cells[0].Value.ToString());

                if (pdb == null)
                {
                    pdb = new ProdutosDB();
                }
                Produto p = pdb.getProdutoByID(pid);

                if (p != null)
                {
                    carr.removeProduto(p);
                    dataGridView3.Rows.RemoveAt(rowindex);
                    dataGridView3.Update();
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Não é possível cancelar uma venda não selecionada.");
            }
            catch (System.InvalidOperationException)
            {
                MessageBox.Show("Não é possível cancelar uma venda não selecionada");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Hide();
            lctrl.Show();
            lctrl.textBox2.Clear();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            form2 = new Form2(this);
            if (this.op.Tipo == 0)
            {
                form2.Show();
            }
            else
            {
                form2.Hide();
                MessageBox.Show("Apenas usuários do tipo 0 podem realizar esta ação.");
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3(cdb, this);
            form3.Show();
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView2.CurrentCell != null)
                {
                    int rowindex = dataGridView2.SelectedCells[0].RowIndex;
                    int pid = Int32.Parse(dataGridView2.Rows[rowindex].Cells[0].Value.ToString());
                    if (pdb == null)
                    {
                        pdb = new ProdutosDB();
                    }
                    Produto p = pdb.getProdutoByID(pid);
                    if (p != null)
                    {
                        if (p.QtdEstoque > 0)
                        {
                            if (carr == null)
                            {
                                carr = new CarrinhoDB(op);
                            }
                            carr.addProduto(p);
                            dataGridView3.DataSource = null;
                            dataGridView3.DataSource = carr.Lista;
                        }
                        else
                        {
                            MessageBox.Show("Produto aguardando reposição em estoque.");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Não é possível cancelar uma venda não selecionada.");
                }
            }
            catch (NullReferenceException)
            {

            }
            catch (System.InvalidOperationException)
            {
                MessageBox.Show("Não é possível cancelar uma venda não selecionada");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            carr.limparCarrinho();
            dataGridView3.DataSource = null;
            dataGridView3.DataSource = carr.Lista;
        }
    }
}
