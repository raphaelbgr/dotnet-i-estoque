﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace estoque
{
    public class CadastroDB
    {
        public Operador Autenticar(string login, string senha)
        {
            using (SqlConnection sqlConn =
                    new SqlConnection("Data Source=(localdb)\\mssqllocaldb;Initial Catalog=estoque;Integrated Security=True"))
            {
                using (SqlCommand sqlCommand = new SqlCommand())
                {
                    sqlCommand.Parameters.AddWithValue("@PLOGIN", login);
                    sqlCommand.Parameters.AddWithValue("@PPASSWORD", senha);

                    sqlCommand.CommandText = "SELECT OPERADOR FROM LOGINS WHERE LOGIN = @PLOGIN AND PASSWORD = @PPASSWORD";
                    sqlCommand.Connection = sqlConn;
                    sqlConn.Open();

                    SqlDataReader dataReader = sqlCommand.ExecuteReader(); //para caso que retorna registros

                    Operador op = null;
                    if (dataReader.Read()) //encontrou registro
                    {
                        string operador_id = dataReader["operador"].ToString();

                        sqlCommand.Parameters.AddWithValue("@OID", operador_id);
                        sqlCommand.CommandText = "SELECT * FROM OPERADORES WHERE ID = @OID";
                        dataReader.Close();
                        dataReader = sqlCommand.ExecuteReader();
                        dataReader.Read();

                        op =  new Operador(
                                Int32.Parse(dataReader["ID"].ToString()),
                                Int32.Parse(dataReader["TIPO"].ToString()),
                                dataReader["NOME"].ToString(),
                                login,
                                senha,
                                dataReader["ENDERECO"].ToString(),
                                dataReader["TELEFONE"].ToString()
                            );
                    }
                    sqlConn.Close(); // Fecha a conexão
                    return op;
                }
            }

        }

        public bool AddOperador(int id, int tipo, string nome, string login, string senha, string endereco, string telefone)
        {
            SqlConnection sqlConn = new SqlConnection("Data Source=(localdb)\\mssqllocaldb;Initial Catalog=estoque;Integrated Security=True");
            SqlCommand sqlCommand = new SqlCommand();
            SqlCommand sqlCommand2 = new SqlCommand();

            bool returnValue;

            try
            {
                Operador op = new Operador();
                op.Id = id;
                op.Tipo = tipo;
                op.Nome = nome;
                op.Login = login;
                op.Senha = senha;
                op.Endereco = endereco;
                op.Telefone = telefone;

                sqlCommand.Parameters.AddWithValue("@ID", op.Id);
                sqlCommand.Parameters.AddWithValue("@TIPO", op.Tipo);
                sqlCommand.Parameters.AddWithValue("@NOME", op.Nome);
                sqlCommand.Parameters.AddWithValue("@ENDERECO", op.Endereco);
                sqlCommand.Parameters.AddWithValue("@TELEFONE", op.Telefone);

                sqlCommand2.Parameters.AddWithValue("@ID", op.Id);
                sqlCommand2.Parameters.AddWithValue("@LOGIN", op.Login);
                sqlCommand2.Parameters.AddWithValue("@SENHA", op.Senha);

                sqlCommand.CommandText = "INSERT INTO OPERADORES" +
                        " VALUES (@ID, @TIPO, @NOME, @ENDERECO, @TELEFONE)";
                sqlCommand2.CommandText = "INSERT INTO LOGINS" +
                        " VALUES (@LOGIN, @SENHA, @ID)";

                sqlCommand.Connection = sqlConn;
                sqlCommand2.Connection = sqlConn;

                sqlCommand.Connection.Open();
   
                if (sqlCommand.ExecuteNonQuery() > 0)
                {
                    sqlCommand.Connection.Close();
                    sqlCommand2.Connection.Open();
                    if (sqlCommand2.ExecuteNonQuery() > 0)
                    {
                        sqlCommand2.Connection.Close();
                        returnValue = true;
                    }
                    else
                    {
                        returnValue = false;
                    }                
                }
                else
                {
                    returnValue = false;
                }
                return returnValue;
            }
            catch (Exception a)
            {
                MessageBox.Show(a.GetType() + " " + a.Message);
                return false;
            }
        }
    }
}
