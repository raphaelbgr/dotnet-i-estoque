﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace estoque
{
    class VendasDB
    {
        public List<Venda> vendas = new List<Venda>();
        public Venda RealizarVenda(CarrinhoDB carr)
        {
            Venda venda = null;

            SqlConnection sqlConn = new SqlConnection("Data Source=(localdb)\\mssqllocaldb;Initial Catalog=estoque;Integrated Security=True");
            try
            {
                venda = new Venda();
                venda.CodOperador = carr.Operador;
                venda.PrecoVenda = carr.Preco;
                venda.Data = DateTime.Now.Date.ToString();
                venda.Hora = DateTime.Now.TimeOfDay.ToString();
                venda.Produtos = carr.ProductsToString();

                SqlCommand sqlCommand = new SqlCommand();
                Random rnd = new Random();
                sqlCommand.Parameters.AddWithValue("COD", rnd.Next(123332, 7238284));
                sqlCommand.Parameters.AddWithValue("CODOPERADOR", venda.CodOperador);
                sqlCommand.Parameters.AddWithValue("PRECOVENDA", venda.PrecoVenda);
                sqlCommand.Parameters.AddWithValue("DATA", venda.Data);
                sqlCommand.Parameters.AddWithValue("HORA", venda.Hora);
                sqlCommand.Parameters.AddWithValue("PRODUTOS", venda.Produtos);
                sqlCommand.CommandText = "INSERT INTO VENDAS VALUES (@COD, @PRODUTOS, @PRECOVENDA, @CODOPERADOR, @DATA, @HORA);";
                sqlCommand.Connection = sqlConn;

                sqlConn.Open();

                if (sqlCommand.ExecuteNonQuery() > 0) //encontrou registro
                {
                    sqlConn.Close(); // Fecha a conexão
                    sqlConn.Open();
                    sqlCommand.CommandText = "SELECT COD FROM VENDAS WHERE OPERADOR = @CODOPERADOR AND PRODUTOS = @PRODUTOS AND PRECOVENDA = @PRECOVENDA " +
                        "AND DATA = @DATA AND HORA = @HORA;";
                    SqlDataReader dataReader = sqlCommand.ExecuteReader(); //para caso que retorna registros
                    if (dataReader.Read())
                    {
                        venda.Codigo = Int32.Parse(dataReader["COD"].ToString());
                        vendas.Add(venda);
                        MessageBox.Show("Venda cadastrada com sucesso.");
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Erro de banco de dados, razão: " + e.Message);
            }
            finally
            {
                sqlConn.Close(); // Fecha a conexão
            }
            return venda;
        }
    }

}
