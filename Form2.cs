﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace estoque
{
    public partial class Form2 : Form
    {
        private PainelController painelController;

        public Form2()
        {
            InitializeComponent();
        }

        public Form2(PainelController painelController)
        {
            this.painelController = painelController;
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            switch (painelController.op.Tipo)
            {
                case 0:
                    SqlConnection sqlConn = new SqlConnection("Data Source=(localdb)\\mssqllocaldb;Initial Catalog=estoque;Integrated Security=True");
                    SqlCommand sqlCommand = new SqlCommand();

                    try
                    {
                        Produto p = new Produto();
                        p.Codigo = Int32.Parse(textBox1.Text);
                        p.Descricao = textBox2.Text;
                        p.Fornecedor = textBox3.Text;
                        p.Validade = textBox4.Text;
                        p.QtdEstoque = Convert.ToInt32(textBox5.Text);
                        p.PrecoForn = Convert.ToDouble(textBox6.Text);
                        p.PrecoFinal = Convert.ToDouble(textBox7.Text);


                        sqlCommand.Parameters.AddWithValue("@COD", p.Codigo);
                        sqlCommand.Parameters.AddWithValue("@DESC", p.Descricao);
                        sqlCommand.Parameters.AddWithValue("@FORN", p.Fornecedor);
                        sqlCommand.Parameters.AddWithValue("@VAL", p.Validade);
                        sqlCommand.Parameters.AddWithValue("@QTD", p.QtdEstoque);
                        sqlCommand.Parameters.AddWithValue("@PRECOFORN", p.PrecoForn);
                        sqlCommand.Parameters.AddWithValue("@PRECOFINAL", p.PrecoFinal);

                        sqlCommand.CommandText = "INSERT INTO PRODUTOS" +
                                " VALUES (@COD, @DESC, @FORN, @VAL, @PRECOFORN, @PRECOFINAL, @QTD)";

                        sqlCommand.Connection = sqlConn;
                        sqlCommand.Connection.Open();

                        if (sqlCommand.ExecuteNonQuery() > 0)
                        {
                            MessageBox.Show("Produto adicionado no banco com sucesso!");
                            clearFields();
                        }
                        else
                        {
                            MessageBox.Show("Erro - O banco não adicionou o produto.");
                        }
                    }
                    catch (Exception a)
                    {
                        MessageBox.Show(a.GetType() + " " + a.Message);
                    }
                    finally
                    {
                        sqlCommand.Connection.Close();
                    }
                    break;

                case 1:
                case 2:
                    MessageBox.Show("Sua conta não está autorizada a fazer esse tipo de operação.");
                    break;
            }
        }

        private void clearFields()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            clearFields();
            this.Hide();
        }
    }
}
